
var changeTrackModal = angular.module('myApp.changeTrackModal', ['ngRoute', 'ui.bootstrap']);

changeTrackModal.controller('changeTrackCtrl', function ($scope, $uibModalInstance, userService, infoService, task, commonsService , datesService) {

    $scope.task = task

    $scope.tracksList = [];
    //фитча - проверка на занятость сотрудников
    $scope.cars = commonsService.userCars.map(function(e){
        if(e.car_tasks) {
            var isBusy = false;
            e.car_tasks.map(function (j) {
                //берем таску и делаем запрос на проверку занятости машины временно выключено 23 44
                //if($scope.task.from && $scope.task.to && j.From && j.To
                //    && new Date($scope.task.from).getTime() < new Date(j.From).getTime() || new Date($scope.task.from).getTime() > new Date(j.To).getTime()
                //    && new Date($scope.task.to).getTime() > new Date(jj.From).getTime() || new Date($scope.task.from).getTime() < new Date(j.To).getTime()){
                //    isBusy = true
                //}
            })
            if(!isBusy) return e;
        } else {
            return e;
        }
    }); //['грузовик/самосвал', 'экскаватор (погрузка снега)', 'роторный погрузчик', 'плужно-щеточная машина', 'плужно-щеточная машина с реагентом'];
    //$scope.tracks = ['ВВП-1', 'ВВП-2' , 'ВВП-3'];
    $scope.tracks = commonsService.tracks;


    $scope.getNowTime = function(){
        return datesService.getTimeStringByDate(new Date());
    }
    $scope.task.from = $scope.getNowTime();

    $scope.setToTime = function(task){
        if(task.trackSelected && task.carSelected) {
            var toDate = new Date(new Date().getTime() + ((task.trackSelected.Area / Number(task.carSelected.Velocity) * 1000 * 60 * 15)))
            task.to = datesService.getTimeStringByDate(toDate); //toDate.getHours()+":"+ toDate.getMinutes()
        }
    }

    $scope.setToTime1 = function(task, track){
        if(task.carSelected && track.trackSelected) {
            var toDate = new Date(new Date().getTime() + ((track.trackSelected.Area/Number(task.carSelected.Velocity) * 1000 * 60 * 15)))
            track.to = datesService.getTimeStringByDate(toDate); //toDate.getHours()+":"+ toDate.getMinutes()
        }

    }


    $scope.addTrack = function (){
        if(!$scope.task.tracksList) $scope.task.tracksList = [];
        var newTrack = {from: $scope.getNowTime() };
        $scope.task.tracksList.push(newTrack);
    }

    $scope.addTrack();

    $scope.saveTask = function(){
        var thisTask = null;
        commonsService.tasks.map(function(e){
            if(e.id_task == $scope.task.id_task){
                thisTask = e;
            }
        })
        thisTask.carsToWork =  [$scope.task.carSelected];
        $scope.task.tracksList.map(function(e){ e.Track_name = e.trackSelected.Track_name})
        thisTask.task_tracks = $scope.task.tracksList;
        thisTask.from = $scope.task.tracksList[0].from;
        thisTask.to = $scope.task.tracksList[0].to;
        var taskTracks = [];
        $scope.task.tracksList.map(function (e){
            if(e){
                taskTracks.push({
                    "track": e.id_tt,
                    "task": $scope.task.id_task,
                    "from": new Date(),
                    "to": new Date(),
                    "carId": $scope.task.carSelected.id_car
                })
            }
        });

        //commonsService.createTaskTracks(taskTracks).then(function(data){
        //    console.log(data)
        //})
//
        //commonsService.updateTask(thisTask.id_task, thisTask.StatusId).then(function(data){
        //    console.log(data)
        //})

        close();
    }

    $scope.close = function () {
        close();
    };

    function  close(){
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
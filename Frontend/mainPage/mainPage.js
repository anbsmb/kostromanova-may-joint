'use strict';

var kanban = angular.module('myApp.mainPage', ['ngRoute']);

kanban.controller('KanbanCtrl', function ($scope, mainService, $window, $rootScope, commonsService, infoService, userService, projectService, datesService ) {

    $scope.user = userService.User;
    $scope.kanbanStatuses = null;
    $scope.projectClassificators = [{class_id:1,class_name:""}];
    $scope.projectClassificator = null;
    $scope.projects = null;
    $scope.curDate = datesService.getSimplyDate(new Date());
    $scope.isSelected = null;

    /* ВСЯ МОКОВАЯ ДАТА  - ДАЛЕЕ ПОЛУЧАЕМ С АПИХИ СЕРВАКА */
    $scope.weather = "-20 ℃ .. -25 ℃";
    var itemsColors = ['text-light']
    var borderColors = [1,2,3] //['bg-secondary', 'bg-info', 'bg-info']

    var boardsStatic = ['СДЕЛАТЬ', 'В РАБОТЕ', 'ГОТОВО'];
    var boards = [];

    var cars = commonsService.cars;
    var userCars = commonsService.userCars;
    var tracks = commonsService.tracks;
    //mock data
    var tasks = commonsService.tasks;

    var arrowUp = "<i title='Высокий приоритет' class='fa fa-arrow-up big-size' style='color: #EB5757'></i>";
    var lines = "<i title='Средний приоритет' class='fas fa-grip-lines big-size' style='color: #F2c94c'></i>";
    var arrowDown = "<i title='Низкий приоритет' class='fa fa-arrow-down big-size' style='color: #219653'></i>";

    $scope.search = {search:""};

    getAllInfo();
    /*получение всех данных для отобрадения канбана*/
    function getAllInfo(){
        getWeather();
        getAllLines();
        getAllTracks();
        getAllTasksByFilter();
        getAllCars();
    }

    function getAllTasksByFilter(){
        getAllTasks();
        var infoInterval = setInterval(function (){
            if($scope.tasks) { //&& $scope.lines && $scope.tracks){
                clearInterval(infoInterval);
                if($scope.prioritySelected.checked1 || $scope.prioritySelected.checked2 || $scope.prioritySelected.checked3) {
                    $scope.tasks = $scope.tasks.filter(function (e) {
                        if ($scope.prioritySelected.checked1 && e.priorityId == 1) return e;
                        if ($scope.prioritySelected.checked2 && e.priorityId == 2) return e;
                        if ($scope.prioritySelected.checked3 && e.priorityId == 3) return e;
                    })
                }
                if($scope.search && $scope.search.search && $scope.search.search.length > 0){
                    $scope.tasks = $scope.tasks.filter(function (e) {
                        if (e.task_name.toLowerCase().indexOf($scope.search.search)>=0) return e;
                    })
                }
                createCanban();
            }
        }, 250);
    }

    function getWeather(){
        userService.getWeather().then(function (data){
            $scope.weather = data[0].temp + " ℃ ... "+data[data.length-1].temp+" ℃";
        })
    }

    function getAllCars(){
        commonsService.getAllCars().then(function (data){
            $scope.cars = data;
            commonsService.userCars = data;
        })
    }

    function getAllTasks(){
        commonsService.getAllTasks().then(function (data){
             //$scope.tasks = tasks;
             //if(!data || !data.length>0) $scope.tasks = tasks;
             $scope.tasks = data;
        }, function (){
            //$scope.tasks = tasks;
        })

    }

    function getAllLines(){
        commonsService.getAllLines().then(function (data){
            commonsService.lines = data;
            $scope.lines = data;
        })
    }

    function getAllTracks(){
        commonsService.getAllTracks().then(function (data){
            commonsService.tracks = data;
            $scope.tracks = data;
        })
    }

    $scope.prioritySelected = {};

    $scope.getAllTasksByFilter = function (){
        getAllTasksByFilter();
    }


    function getIconByPriority(priorityId) {
        var icon = arrowDown;
        if(priorityId == 1){ icon = arrowUp; }
        if(priorityId == 2){ icon =  lines; }
        if(priorityId == 3){ icon = arrowDown; }
        return icon;
    }

    /*формирование карточки канбана*/
    function getInfo(tsk){
        var info = "<span>";
        info += getIconByPriority(tsk.priorityId) + " <b class='bold text-dark'>" + tsk.task_name + "</b><div class='float-right small'>";
        if(tsk.From && tsk.To) {
            info += datesService.getTimeStringByDate(new Date(tsk.From)) +" - " + datesService.getTimeStringByDate(new Date(tsk.To))
        }
        info += "</div><br>" ;
        if(tsk.cars && tsk.cars.length>0)
            info += "<div class='mt-3'><span class='small mt-3'>Требуются машины: </span><b class='bold small'>"+ tsk.cars.map(function (e){return e.Name_type}).join(", ") +" </b></div> " ;
        if(tsk.task_tracks && tsk.task_tracks.length>0)
            info += "<div class='mt-3'><span class='small mt-3'>Участки: </span><b class='bold small'>" + tsk.task_tracks.map(function(e){ return e.Track + ((e.from && e.from ) ? (' с ' + e.from + ' по ' + e.to) : ' (не назначено)')}).join(", ") +" </b></div>" ;
        if(tsk.carsToWork && tsk.carsToWork.length>0)
            info += "<div class='mt-3'><span class='small mt-3'>Назначенные машины: <b class='bold'>" + tsk.carsToWork.map(function(e){ return e.Car_name}).join(", ") +"</b></span></div> " ;
        return info+"</span>";
    }

    function createCanban(){
        tryDigest();
        boards = [];
        angular.forEach(boardsStatic, function (boardName, index){
            var tasks = [];
            angular.forEach($scope.tasks, function (tsk) {
                if(tsk.StatusId == index+1) {
                    var task = {
                        id: tsk.id_task,
                        title: getInfo(tsk),
                        //click: openTask,
                        drop: changeStatus,
                        class: [itemsColors[tsk.StatusId]]
                    };
                    tasks.push(task)
                }
            });

            var board = {
                id: index,
                title: boardName,
                class: borderColors[index]+",text-light,pointer",
                item: tasks

            };
            boards.push(board);
        })
        setTimeout(function (){
            const myNode = document.getElementById("myKanban");
            myNode.textContent = '';
            var KanbanTest = new jKanban({
                element: "#myKanban",
                gutter: "10px",
                widthBoard: "32.05%",
                itemHandleOptions:{
                    enabled: true,
                },
                click: function(el) {
                    //console.log("Trigger on all items click!");
                },
                dropEl: function(el, target, source, sibling){
                    //console.log(target.parentElement.getAttribute('data-id'));
                    //console.log(el, target, source, sibling)
                },
                buttonClick: function(el, boardId) {
                    //createProjectWithKanbanStatus(boardId);
                },
                addItemButton: true,
                boards:  boards
            });

        },500)
    }


    function tryDigest() {
        if (!$rootScope.$$phase) {
            $rootScope.$apply();
        }
    }

    function getTrackById(taskId){
        var task = null;
        if($scope.tasks && taskId && Number(taskId)) $scope.tasks.map(function(e){ if(e.id_task == Number(taskId)) task = e; })
        return task;
    }

    var changeStatus = function(el, target){
        console.log(el)
        if($(el)[0] && $(el)[0].getAttribute('data-eid') && $(el)[0].childNodes[0] && $(el)[0].childNodes[0].getAttribute('id')) {
            if($(el)[0].getAttribute('data-eid') && Number($(el)[0].getAttribute('data-eid'))) {
                var task = getTrackById($(el)[0].childNodes[0].getAttribute('id'));

                if (task && task.StatusId==1) {
                    task.StatusId = 2;
                    //getAllTasksByFilter();
                    projectService.changeTrackModal(task).then(function () {
                        getAllTasksByFilter();
                    });
                } else {
                    task.StatusId = Number($($(el)[0].parentNode)[0].parentNode.getAttribute('data-id')) + 1;
                   // infoService.infoFunction("Невозможно изменить задачу: нет track по trackId в списке", "Ошибка");
                }
            }
        } else
            infoService.infoFunction("Невозможно изменить задачу: нет data eid", "Ошибка");
    }

});
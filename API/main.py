from flask import Flask, request
from flask_cors import CORS, cross_origin
from yaweather import Russia, YaWeather
import psycopg2
from psycopg2.extras import execute_values
import datetime
import numpy as np
import json, requests
import warnings
import threading
warnings.filterwarnings("ignore")

conn = psycopg2.connect(host="134.209.236.228", port="5432", user="knova", password="hack21052021",
                        database="postgres")

app = Flask(__name__)
CORS(app, support_credentials=True)
Ykey = '178fb69f-e5b3-4a09-8927-e024979232a8'

c1 = 55.97250710300642
c2 = 37.413000366210945
Sher_coordinates = (c1, c2)

url = 'https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/karta_sher.json'
resp = requests.get(url)
data2 = json.loads(resp.text)

# url = 'https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/GeoJSON_sher.json'
# resp = requests.get(url)
# data = json.loads(resp.text)

# Время, через которое надо обновлять данные о погоде в БД
WAIT_SECONDS = 3600


def set_weather():
    y = YaWeather(api_key=Ykey)
    resw = y.forecast(coordinates=Sher_coordinates,lang='ru_RU')
    hr=datetime.datetime.today().hour
    tmp=resw.fact.temp
    pt=resw.fact.prec_type
    ps=resw.fact.prec_strength
    sqll='UPDATE public."Weather" SET temp={}, prec_type={}, prec_strength={} '.format(tmp,pt,ps)
    dd=0
    hh=hr
    for i in range (4):
        hh+=1
        if hh==24:
            hh=0
            dd+=1
        tmp=dict(resw.forecasts[dd])['hours'][hh].temp
        pt=dict(resw.forecasts[dd])['hours'][hh].prec_type
        ps=dict(resw.forecasts[dd])['hours'][hh].prec_strength
        sqll=sqll+', temp{0}={1}, prec_type{0}={2}, prec_strength{0}={3}'.format(i+1,tmp,pt,ps)
    sqll=sqll+' WHERE id=1'
    cur = conn.cursor()
    cur.execute(sqll)
    conn.commit()
    cur.close()

    threading.Timer(WAIT_SECONDS, set_weather).start()


def put_snow():
    """
    Каждый час проверяет погоду и при наличии снега добавляет его на трэки
    """
    snow_prec_type = [2, 3, 4]
    cur = conn.cursor()
    cur.execute('SELECT temp, prec_type, prec_strength FROM "Weather"')
    temp, prec_type, amount = cur.fetchone()

    cur.execute('SELECT "Track_name", "Snow_height" FROM "Track"')
    tracks = cur.fetchall()
    ids = list(map(lambda track: track[0], tracks))
    existing = list(map(lambda track: track[1], tracks))

    if prec_type in snow_prec_type:
        print(amount)
        execute_values(cur, """
            UPDATE "Track"
            SET "Snow_height" = update_upload.already_has + update_upload.amount
            FROM (VALUES %s) AS update_upload (name, already_has, amount)
            WHERE "Track"."Track_name" = update_upload.name
        """, list(zip(ids, existing, [amount] * len(ids))))

        conn.commit()
    cur.close()

    threading.Timer(WAIT_SECONDS, put_snow).start()


@app.route('/api/getPolygon')
@cross_origin(supports_credentials=True)
def get_polygon():
    """
    Определить название трека по координатам
    """
    x0 = request.args.get('x0', type=float)
    y0 = request.args.get('y0', type=float)
    for k in data2:
        ss=np.array(data2[k]).T
        x1=ss[0][1:]
        x2=ss[0][:-1]
        y1=ss[1][1:]
        y2=ss[1][:-1]
        xt=(y0-y1)/(y2-y1)*(x2-x1)+x1
        if np.sum(((xt<=x0)&((x2-xt)*(xt-x1)>=0) & ((y2-y0)*(y0-y1)>=0)) | ((y1==y0) & (y2==y0) & (((x1-x0)*(x0-x2))>0)))%2==1:
            return k
    return ''


@app.route('/api/getWeather')
@cross_origin("*")
def get_weather():
    cur = conn.cursor()
    cur.execute("""
        SELECT * FROM "Weather"
    """)
    _, temp, temp1, temp2, temp3, temp4, prec_type, \
        prec_type1, prec_type2, prec_type3, prec_type4, \
        prec_strength, prec_strength1, prec_strength2, prec_strength3, prec_strength4 = cur.fetchone()
    cur.close()

    res = json.dumps([
        {'temp': temp, 'prec_type': prec_type, 'prec_strength': prec_strength},
        {'temp': temp1, 'prec_type': prec_type1, 'prec_strength': prec_strength1},
        {'temp': temp2, 'prec_type': prec_type2, 'prec_strength': prec_strength2},
        {'temp': temp3, 'prec_type': prec_type3, 'prec_strength': prec_strength3},
        {'temp': temp4, 'prec_type': prec_type4, 'prec_strength': prec_strength4}
    ])
    return res


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8100', debug=True)

    # Запустить по готовности
    # set_weather()
    # put_snow()
const {Router} = require('express');
import {db, wrapResponse, handleDefault} from '../utils';
const router = Router();

// /api/tasks/getAll
router.get(
    '/getAll',
    wrapResponse((request, response) => {
        let allTasks = null;
        let taskIds = null;
        let task_tracks = null;
        request.pool.connect()
            .then(client => {
                return client
                    .query(db.queries.select('Task'))
                    .then(db.getAll)
                    .then((tasks) => {
                        allTasks = tasks;
                        taskIds = tasks.map((task) => task.id_task)
                        if (taskIds.length) {
                            return client.query(db.queries.select('Task_tracks', null, null,
                                null, `WHERE t."Task" IN (${taskIds.join(', ')})`, false));
                        }
                        return { rows: [] };
                    })
                    .then(db.getAll)
                    .then((result) => {
                        task_tracks = result;
                        if (taskIds.length) {
                            return client.query(db.queries.select('Task_cars', null, 'ct.*',
                                'LEFT JOIN "Car_type" ct ON ct."id_type" = t."Car_type_id"', `WHERE t."Task" IN (${taskIds.join(', ')})`, false));
                        }
                        return { rows: [] };
                    })
                    .then(db.getAll)
                    .then((task_cars) => {
                        const getCarsInfoByTaskId = (taskId, filterFunc) =>
                            task_cars.filter((info) => info.Task === taskId)
                                     .filter(filterFunc)
                        const res = allTasks.map((task) => ({
                            ...task,
                            task_tracks: task_tracks.filter((info) => info.Task === task.id_task)
                                                    .map((info) => ({
                                                        Track: info.Track,
                                                        To: info.To,
                                                        From: info.From
                                                    })),
                            cars: getCarsInfoByTaskId(task.id_task, (task_car) => !task_car.Cars),
                            carsToWork: getCarsInfoByTaskId(task.id_task, (task_car) => task_car.Cars)
                        }));
                        client.release();
                        response.json(res);
                    })
                    .catch((e) => {
                        client.release();
                        handleDefault(e, response);
                    });
            });
    })
);

module.exports = router;
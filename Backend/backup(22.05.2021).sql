--
-- PostgreSQL database dump
--

-- Dumped from database version 12.6 (Ubuntu 12.6-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 13.0

-- Started on 2021-05-22 14:10:49

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 214 (class 1259 OID 16982)
-- Name: Car; Type: TABLE; Schema: public; Owner: knova
--

CREATE TABLE public."Car" (
    id_car integer NOT NULL,
    "Car_name" character varying(30),
    "Driver_id" integer,
    "Car_type" integer
);


ALTER TABLE public."Car" OWNER TO knova;

--
-- TOC entry 213 (class 1259 OID 16980)
-- Name: Car_id_car_seq; Type: SEQUENCE; Schema: public; Owner: knova
--

CREATE SEQUENCE public."Car_id_car_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Car_id_car_seq" OWNER TO knova;

--
-- TOC entry 3086 (class 0 OID 0)
-- Dependencies: 213
-- Name: Car_id_car_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: knova
--

ALTER SEQUENCE public."Car_id_car_seq" OWNED BY public."Car".id_car;


--
-- TOC entry 208 (class 1259 OID 16955)
-- Name: Car_type; Type: TABLE; Schema: public; Owner: knova
--

CREATE TABLE public."Car_type" (
    id_type integer NOT NULL,
    "Name_type" character varying(30),
    "Logo" character varying,
    "Velocity" numeric
);


ALTER TABLE public."Car_type" OWNER TO knova;

--
-- TOC entry 207 (class 1259 OID 16953)
-- Name: Car_type_id_type_seq; Type: SEQUENCE; Schema: public; Owner: knova
--

CREATE SEQUENCE public."Car_type_id_type_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Car_type_id_type_seq" OWNER TO knova;

--
-- TOC entry 3087 (class 0 OID 0)
-- Dependencies: 207
-- Name: Car_type_id_type_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: knova
--

ALTER SEQUENCE public."Car_type_id_type_seq" OWNED BY public."Car_type".id_type;


--
-- TOC entry 224 (class 1259 OID 17022)
-- Name: Coordinates; Type: TABLE; Schema: public; Owner: knova
--

CREATE TABLE public."Coordinates" (
    id_c integer NOT NULL,
    "DT" timestamp without time zone,
    "ID" integer,
    "Type" integer,
    "Lat" double precision,
    "Long" double precision
);


ALTER TABLE public."Coordinates" OWNER TO knova;

--
-- TOC entry 223 (class 1259 OID 17020)
-- Name: Coordinates_id_c_seq; Type: SEQUENCE; Schema: public; Owner: knova
--

CREATE SEQUENCE public."Coordinates_id_c_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Coordinates_id_c_seq" OWNER TO knova;

--
-- TOC entry 3088 (class 0 OID 0)
-- Dependencies: 223
-- Name: Coordinates_id_c_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: knova
--

ALTER SEQUENCE public."Coordinates_id_c_seq" OWNED BY public."Coordinates".id_c;


--
-- TOC entry 222 (class 1259 OID 17014)
-- Name: Ingeneers; Type: TABLE; Schema: public; Owner: knova
--

CREATE TABLE public."Ingeneers" (
    id_ing integer NOT NULL,
    user_id integer
);


ALTER TABLE public."Ingeneers" OWNER TO knova;

--
-- TOC entry 221 (class 1259 OID 17012)
-- Name: Ingeneers_id_ing_seq; Type: SEQUENCE; Schema: public; Owner: knova
--

CREATE SEQUENCE public."Ingeneers_id_ing_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Ingeneers_id_ing_seq" OWNER TO knova;

--
-- TOC entry 3089 (class 0 OID 0)
-- Dependencies: 221
-- Name: Ingeneers_id_ing_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: knova
--

ALTER SEQUENCE public."Ingeneers_id_ing_seq" OWNED BY public."Ingeneers".id_ing;


--
-- TOC entry 202 (class 1259 OID 16927)
-- Name: Line; Type: TABLE; Schema: public; Owner: knova
--

CREATE TABLE public."Line" (
    "Line_name" character varying(30) NOT NULL,
    "Type" boolean
);


ALTER TABLE public."Line" OWNER TO knova;

--
-- TOC entry 206 (class 1259 OID 16945)
-- Name: Plane; Type: TABLE; Schema: public; Owner: knova
--

CREATE TABLE public."Plane" (
    plane_id integer NOT NULL,
    "Plane_name" character varying(30),
    "Logo" character varying
);


ALTER TABLE public."Plane" OWNER TO knova;

--
-- TOC entry 205 (class 1259 OID 16939)
-- Name: Plane_reserve; Type: TABLE; Schema: public; Owner: knova
--

CREATE TABLE public."Plane_reserve" (
    id_reserve integer NOT NULL,
    "Plane" integer,
    "From" timestamp without time zone,
    "To" timestamp without time zone,
    "Line_name" character varying(30),
    "Track_name" character varying(30)
);


ALTER TABLE public."Plane_reserve" OWNER TO knova;

--
-- TOC entry 204 (class 1259 OID 16937)
-- Name: Plane_reserve_id_reserve_seq; Type: SEQUENCE; Schema: public; Owner: knova
--

CREATE SEQUENCE public."Plane_reserve_id_reserve_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Plane_reserve_id_reserve_seq" OWNER TO knova;

--
-- TOC entry 3090 (class 0 OID 0)
-- Dependencies: 204
-- Name: Plane_reserve_id_reserve_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: knova
--

ALTER SEQUENCE public."Plane_reserve_id_reserve_seq" OWNED BY public."Plane_reserve".id_reserve;


--
-- TOC entry 212 (class 1259 OID 16974)
-- Name: Rule_cars; Type: TABLE; Schema: public; Owner: knova
--

CREATE TABLE public."Rule_cars" (
    id_rc integer NOT NULL,
    "Car_type" integer,
    id_rule integer
);


ALTER TABLE public."Rule_cars" OWNER TO knova;

--
-- TOC entry 211 (class 1259 OID 16972)
-- Name: Rule_cars_id_rc_seq; Type: SEQUENCE; Schema: public; Owner: knova
--

CREATE SEQUENCE public."Rule_cars_id_rc_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Rule_cars_id_rc_seq" OWNER TO knova;

--
-- TOC entry 3091 (class 0 OID 0)
-- Dependencies: 211
-- Name: Rule_cars_id_rc_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: knova
--

ALTER SEQUENCE public."Rule_cars_id_rc_seq" OWNED BY public."Rule_cars".id_rc;


--
-- TOC entry 210 (class 1259 OID 16966)
-- Name: Rules; Type: TABLE; Schema: public; Owner: knova
--

CREATE TABLE public."Rules" (
    id_rule integer NOT NULL,
    velocity double precision,
    "Snow_height_min" double precision,
    "Snow_height_max" double precision,
    "Roll_min" double precision,
    "Roll_max" double precision,
    "Clutch_min" double precision,
    "Clutch_max" double precision,
    "Ing" boolean,
    "Val_height_min" double precision,
    "Val_height_max" double precision,
    "Type_line" boolean
);


ALTER TABLE public."Rules" OWNER TO knova;

--
-- TOC entry 209 (class 1259 OID 16964)
-- Name: Rules_id_rule_seq; Type: SEQUENCE; Schema: public; Owner: knova
--

CREATE SEQUENCE public."Rules_id_rule_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Rules_id_rule_seq" OWNER TO knova;

--
-- TOC entry 3092 (class 0 OID 0)
-- Dependencies: 209
-- Name: Rules_id_rule_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: knova
--

ALTER SEQUENCE public."Rules_id_rule_seq" OWNED BY public."Rules".id_rule;


--
-- TOC entry 216 (class 1259 OID 16990)
-- Name: Task; Type: TABLE; Schema: public; Owner: knova
--

CREATE TABLE public."Task" (
    id_task integer NOT NULL,
    "From" timestamp without time zone,
    "To" timestamp without time zone,
    "Accepted" boolean,
    "Author" integer
);


ALTER TABLE public."Task" OWNER TO knova;

--
-- TOC entry 218 (class 1259 OID 16998)
-- Name: Task_cars; Type: TABLE; Schema: public; Owner: knova
--

CREATE TABLE public."Task_cars" (
    id_tc integer NOT NULL,
    "Task" integer,
    "Car" integer
);


ALTER TABLE public."Task_cars" OWNER TO knova;

--
-- TOC entry 217 (class 1259 OID 16996)
-- Name: Task_cars_id_tc_seq; Type: SEQUENCE; Schema: public; Owner: knova
--

CREATE SEQUENCE public."Task_cars_id_tc_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Task_cars_id_tc_seq" OWNER TO knova;

--
-- TOC entry 3093 (class 0 OID 0)
-- Dependencies: 217
-- Name: Task_cars_id_tc_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: knova
--

ALTER SEQUENCE public."Task_cars_id_tc_seq" OWNED BY public."Task_cars".id_tc;


--
-- TOC entry 215 (class 1259 OID 16988)
-- Name: Task_id_task_seq; Type: SEQUENCE; Schema: public; Owner: knova
--

CREATE SEQUENCE public."Task_id_task_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Task_id_task_seq" OWNER TO knova;

--
-- TOC entry 3094 (class 0 OID 0)
-- Dependencies: 215
-- Name: Task_id_task_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: knova
--

ALTER SEQUENCE public."Task_id_task_seq" OWNED BY public."Task".id_task;


--
-- TOC entry 220 (class 1259 OID 17006)
-- Name: Task_tracks; Type: TABLE; Schema: public; Owner: knova
--

CREATE TABLE public."Task_tracks" (
    id_tt integer NOT NULL,
    "Track" character varying(30),
    "Task" integer,
    "From" timestamp without time zone,
    "To" timestamp without time zone
);


ALTER TABLE public."Task_tracks" OWNER TO knova;

--
-- TOC entry 219 (class 1259 OID 17004)
-- Name: Task_tracks_id_tt_seq; Type: SEQUENCE; Schema: public; Owner: knova
--

CREATE SEQUENCE public."Task_tracks_id_tt_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Task_tracks_id_tt_seq" OWNER TO knova;

--
-- TOC entry 3095 (class 0 OID 0)
-- Dependencies: 219
-- Name: Task_tracks_id_tt_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: knova
--

ALTER SEQUENCE public."Task_tracks_id_tt_seq" OWNED BY public."Task_tracks".id_tt;


--
-- TOC entry 203 (class 1259 OID 16932)
-- Name: Track; Type: TABLE; Schema: public; Owner: knova
--

CREATE TABLE public."Track" (
    "Track_name" character varying(30) NOT NULL,
    "Line" character varying(30),
    "Last_clearning" timestamp without time zone,
    "Snow_height" integer,
    "Roll" double precision,
    "Clutch" double precision,
    "Area" double precision,
    "Val" double precision,
    "Ing" boolean
);


ALTER TABLE public."Track" OWNER TO knova;

--
-- TOC entry 227 (class 1259 OID 17036)
-- Name: Users; Type: TABLE; Schema: public; Owner: knova
--

CREATE TABLE public."Users" (
    user_id integer NOT NULL,
    "Name" character varying(30),
    "Port" integer
);


ALTER TABLE public."Users" OWNER TO knova;

--
-- TOC entry 226 (class 1259 OID 17030)
-- Name: Weather; Type: TABLE; Schema: public; Owner: knova
--

CREATE TABLE public."Weather" (
    id integer NOT NULL,
    temp double precision,
    temp1 double precision,
    temp2 double precision,
    temp3 double precision,
    temp4 double precision,
    prec_type integer,
    prec_type1 integer,
    prec_type2 integer,
    prec_type3 integer,
    prec_type4 integer,
    prec_strength double precision,
    prec_strength1 double precision,
    prec_strength2 double precision,
    prec_strength3 double precision,
    prec_strength4 double precision
);


ALTER TABLE public."Weather" OWNER TO knova;

--
-- TOC entry 225 (class 1259 OID 17028)
-- Name: Weather_id_seq; Type: SEQUENCE; Schema: public; Owner: knova
--

CREATE SEQUENCE public."Weather_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Weather_id_seq" OWNER TO knova;

--
-- TOC entry 3096 (class 0 OID 0)
-- Dependencies: 225
-- Name: Weather_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: knova
--

ALTER SEQUENCE public."Weather_id_seq" OWNED BY public."Weather".id;


--
-- TOC entry 2878 (class 2604 OID 16985)
-- Name: Car id_car; Type: DEFAULT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Car" ALTER COLUMN id_car SET DEFAULT nextval('public."Car_id_car_seq"'::regclass);


--
-- TOC entry 2875 (class 2604 OID 16958)
-- Name: Car_type id_type; Type: DEFAULT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Car_type" ALTER COLUMN id_type SET DEFAULT nextval('public."Car_type_id_type_seq"'::regclass);


--
-- TOC entry 2883 (class 2604 OID 17025)
-- Name: Coordinates id_c; Type: DEFAULT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Coordinates" ALTER COLUMN id_c SET DEFAULT nextval('public."Coordinates_id_c_seq"'::regclass);


--
-- TOC entry 2882 (class 2604 OID 17017)
-- Name: Ingeneers id_ing; Type: DEFAULT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Ingeneers" ALTER COLUMN id_ing SET DEFAULT nextval('public."Ingeneers_id_ing_seq"'::regclass);


--
-- TOC entry 2874 (class 2604 OID 16942)
-- Name: Plane_reserve id_reserve; Type: DEFAULT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Plane_reserve" ALTER COLUMN id_reserve SET DEFAULT nextval('public."Plane_reserve_id_reserve_seq"'::regclass);


--
-- TOC entry 2877 (class 2604 OID 16977)
-- Name: Rule_cars id_rc; Type: DEFAULT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Rule_cars" ALTER COLUMN id_rc SET DEFAULT nextval('public."Rule_cars_id_rc_seq"'::regclass);


--
-- TOC entry 2876 (class 2604 OID 16969)
-- Name: Rules id_rule; Type: DEFAULT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Rules" ALTER COLUMN id_rule SET DEFAULT nextval('public."Rules_id_rule_seq"'::regclass);


--
-- TOC entry 2879 (class 2604 OID 16993)
-- Name: Task id_task; Type: DEFAULT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Task" ALTER COLUMN id_task SET DEFAULT nextval('public."Task_id_task_seq"'::regclass);


--
-- TOC entry 2880 (class 2604 OID 17001)
-- Name: Task_cars id_tc; Type: DEFAULT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Task_cars" ALTER COLUMN id_tc SET DEFAULT nextval('public."Task_cars_id_tc_seq"'::regclass);


--
-- TOC entry 2881 (class 2604 OID 17009)
-- Name: Task_tracks id_tt; Type: DEFAULT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Task_tracks" ALTER COLUMN id_tt SET DEFAULT nextval('public."Task_tracks_id_tt_seq"'::regclass);


--
-- TOC entry 2884 (class 2604 OID 17033)
-- Name: Weather id; Type: DEFAULT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Weather" ALTER COLUMN id SET DEFAULT nextval('public."Weather_id_seq"'::regclass);


--
-- TOC entry 3067 (class 0 OID 16982)
-- Dependencies: 214
-- Data for Name: Car; Type: TABLE DATA; Schema: public; Owner: knova
--

COPY public."Car" (id_car, "Car_name", "Driver_id", "Car_type") FROM stdin;
0	Машина 0	\N	3
1	Машина 1	\N	1
2	Машина 2	\N	2
3	Машина 3	\N	0
4	Машина 4	\N	0
5	Машина 5	\N	2
6	Машина 6	\N	0
7	Машина 7	\N	4
8	Машина 8	\N	2
9	Машина 9	\N	3
10	Машина 10	\N	1
11	Машина 11	\N	2
12	Машина 12	\N	1
13	Машина 13	\N	0
14	Машина 14	\N	3
15	Машина 15	\N	0
16	Машина 16	\N	3
17	Машина 17	\N	4
18	Машина 18	\N	2
19	Машина 19	\N	0
20	Машина 20	\N	2
21	Машина 21	\N	4
22	Машина 22	\N	1
23	Машина 23	\N	0
24	Машина 24	\N	4
25	Машина 25	\N	3
26	Машина 26	\N	0
27	Машина 27	\N	3
28	Машина 28	\N	4
29	Машина 29	\N	1
\.


--
-- TOC entry 3061 (class 0 OID 16955)
-- Dependencies: 208
-- Data for Name: Car_type; Type: TABLE DATA; Schema: public; Owner: knova
--

COPY public."Car_type" (id_type, "Name_type", "Logo", "Velocity") FROM stdin;
0	грузовик/самосвал	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/bus-1.png	33
1	экскаватор (погрузка снега)	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/bus.png	55
2	роторный погрузчик	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/bus-1.png	33
3	плужно-щеточная машина	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/oil-truck.png	11
4	плужно-щеточная машина (реаг)	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/fuel-truck.png	54
\.


--
-- TOC entry 3077 (class 0 OID 17022)
-- Dependencies: 224
-- Data for Name: Coordinates; Type: TABLE DATA; Schema: public; Owner: knova
--

COPY public."Coordinates" (id_c, "DT", "ID", "Type", "Lat", "Long") FROM stdin;
1	\N	1	2	55.97136	37.42
2	\N	2	2	55.967984323542076	37.41208791732789
3	\N	3	2	55.96892699413906	37.408336400985725
4	\N	4	2	55.97993333756118	37.439941763877876
5	\N	5	2	55.9687819077068	37.41479873657227
6	\N	6	2	55.97239033586584	37.39261150360108
7	\N	0	1	55.97266351636473	37.3856806755066
8	\N	1	1	55.980319517845096	37.42674350738526
9	\N	5	1	55.976127514647395	37.428488731384284
10	\N	10	1	55.972643503919066	37.38573074340821
11	\N	12	1	55.97426646780152	37.405287623405464
12	\N	6	1	55.96685747013015	37.40418255329133
13	\N	4	1	55.96881792288323	37.403533458709724
14	\N	25	1	55.97690592051964	37.423263788223274
15	\N	28	1	55.97665877441197	37.39700227975846
16	\N	17	1	55.97179296476542	37.43205428123475
17	\N	21	1	55.978357637089445	37.41490602493287
18	\N	22	1	55.9704353131756	37.42673054337502
19	\N	23	1	55.97247739056415	37.43009805679322
20	\N	2	1	55.96737386663355	37.388298511505134
21	\N	3	1	55.97662777218397	37.42823839187623
22	\N	7	1	55.97713601896838	37.415490746498115
23	\N	8	1	55.97775634684633	37.42213189601899
24	\N	9	1	55.963254523586514	37.39854186773301
25	\N	11	1	55.97888588408336	37.42743730545045
26	\N	29	1	55.97449761300027	37.3980724811554
27	\N	27	1	55.97543113767361	37.4420714378357
28	\N	26	1	55.97713753699121	37.41830170154572
29	\N	19	1	55.96937633284599	37.421372830867774
30	\N	24	1	55.96709165143259	37.40013599395753
31	\N	20	1	55.968038353445706	37.39237546920777
34	\N	2	0	55.9771510435709	37.421214580535896
32	\N	0	0	55.9752138955761	37.4216215474701
33	\N	1	0	55.97280834578056	37.39399809255601
35	\N	3	0	55.9746636414548	37.43584871292115
\.


--
-- TOC entry 3075 (class 0 OID 17014)
-- Dependencies: 222
-- Data for Name: Ingeneers; Type: TABLE DATA; Schema: public; Owner: knova
--

COPY public."Ingeneers" (id_ing, user_id) FROM stdin;
1	1
2	10
3	3
4	2
5	7
6	6
7	5
8	4
9	8
\.


--
-- TOC entry 3055 (class 0 OID 16927)
-- Dependencies: 202
-- Data for Name: Line; Type: TABLE DATA; Schema: public; Owner: knova
--

COPY public."Line" ("Line_name", "Type") FROM stdin;
TWY16	f
Main TWY1	f
TWY29	f
N7	f
TWY33	f
B	f
TWY-5	f
L2	f
diag2	f
81ABC	f
N8	f
TWY24	f
TWY-15	f
TWY23	f
TWY4	f
TWY18	f
TWY35	f
D4	f
D2	f
TWY2	f
TWY28	f
TWY27	f
F1	f
F3	f
06C/24C	t
73	f
TWY30	f
TWY34	f
TWY1	f
TWY-14	f
L1	f
TWY-17	f
TWY6A	f
TWY10	f
TWY-19	f
D31	f
F2	f
diag3	f
TWY13	f
Diag1	f
TWY-26	f
TWY11	f
TWY12	f
TWY31	f
TWY-20	f
TWY22	f
TWY3	f
N6	f
TWY6	f
D1	f
Main TWY2	f
D3	f
obezd	f
N3	f
D5	f
06C/24L	t
\.


--
-- TOC entry 3059 (class 0 OID 16945)
-- Dependencies: 206
-- Data for Name: Plane; Type: TABLE DATA; Schema: public; Owner: knova
--

COPY public."Plane" (plane_id, "Plane_name", "Logo") FROM stdin;
0	Аэроплан А000	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
1	Аэроплан А001	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
2	Аэроплан А002	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
3	Аэроплан А003	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
4	Аэроплан А004	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
5	Аэроплан А005	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
6	Аэроплан А006	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
7	Аэроплан А007	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
8	Аэроплан А008	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
9	Аэроплан А009	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
10	Аэроплан А010	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
11	Аэроплан А011	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
12	Аэроплан А012	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
13	Аэроплан А013	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
14	Аэроплан А014	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
15	Аэроплан А015	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
16	Аэроплан А016	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
17	Аэроплан А017	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
18	Аэроплан А018	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
19	Аэроплан А019	https://gitlab.com/anbsmb/kostromanova-may-middleware/-/raw/master/assets/plane.png
\.


--
-- TOC entry 3058 (class 0 OID 16939)
-- Dependencies: 205
-- Data for Name: Plane_reserve; Type: TABLE DATA; Schema: public; Owner: knova
--

COPY public."Plane_reserve" (id_reserve, "Plane", "From", "To", "Line_name", "Track_name") FROM stdin;
\.


--
-- TOC entry 3065 (class 0 OID 16974)
-- Dependencies: 212
-- Data for Name: Rule_cars; Type: TABLE DATA; Schema: public; Owner: knova
--

COPY public."Rule_cars" (id_rc, "Car_type", id_rule) FROM stdin;
\.


--
-- TOC entry 3063 (class 0 OID 16966)
-- Dependencies: 210
-- Data for Name: Rules; Type: TABLE DATA; Schema: public; Owner: knova
--

COPY public."Rules" (id_rule, velocity, "Snow_height_min", "Snow_height_max", "Roll_min", "Roll_max", "Clutch_min", "Clutch_max", "Ing", "Val_height_min", "Val_height_max", "Type_line") FROM stdin;
\.


--
-- TOC entry 3069 (class 0 OID 16990)
-- Dependencies: 216
-- Data for Name: Task; Type: TABLE DATA; Schema: public; Owner: knova
--

COPY public."Task" (id_task, "From", "To", "Accepted", "Author") FROM stdin;
\.


--
-- TOC entry 3071 (class 0 OID 16998)
-- Dependencies: 218
-- Data for Name: Task_cars; Type: TABLE DATA; Schema: public; Owner: knova
--

COPY public."Task_cars" (id_tc, "Task", "Car") FROM stdin;
\.


--
-- TOC entry 3073 (class 0 OID 17006)
-- Dependencies: 220
-- Data for Name: Task_tracks; Type: TABLE DATA; Schema: public; Owner: knova
--

COPY public."Task_tracks" (id_tt, "Track", "Task", "From", "To") FROM stdin;
\.


--
-- TOC entry 3056 (class 0 OID 16932)
-- Dependencies: 203
-- Data for Name: Track; Type: TABLE DATA; Schema: public; Owner: knova
--

COPY public."Track" ("Track_name", "Line", "Last_clearning", "Snow_height", "Roll", "Clutch", "Area", "Val", "Ing") FROM stdin;
06C/24C-4	06C/24C	\N	0	0	0	100	2	t
06C/24L-4	06C/24L	\N	0	0	0	100	20	t
06C/24C-1	06C/24C	\N	0	0	0	100	0	f
06C/24C-2	06C/24C	\N	0	0	0	100	0	f
06C/24C-3	06C/24C	\N	0	0	0	100	0	f
06C/24C-5	06C/24C	\N	0	0	0	100	0	f
06C/24C-6	06C/24C	\N	0	0	0	100	0	f
06C/24L-6	06C/24L	\N	0	0	0	100	0	f
06C/24L-5	06C/24L	\N	0	0	0	100	0	f
06C/24L-3	06C/24L	\N	0	0	0	100	0	f
06C/24L-2	06C/24L	\N	0	0	0	100	0	f
06C/24L-1	06C/24L	\N	0	0	0	100	0	f
TWY11	TWY11	\N	0	0	0	100	0	f
TWY1-2	TWY1	\N	0	0	0	100	0	f
TWY1-1	TWY1	\N	0	0	0	100	0	f
TWY16-2	TWY16	\N	0	0	0	100	0	f
TWY16-1	TWY16	\N	0	0	0	100	0	f
B-1	B	\N	0	0	0	100	0	f
Main TWY1-1	Main TWY1	\N	0	0	0	100	0	f
Main TWY1-2	Main TWY1	\N	0	0	0	100	0	f
TWY-17	TWY-17	\N	0	0	0	100	0	f
N3	N3	\N	0	0	0	100	0	f
TWY6	TWY6	\N	0	0	0	100	0	f
Main TWY1-3	Main TWY1	\N	0	0	0	100	0	f
TWY6A	TWY6A	\N	0	0	0	100	0	f
B-2	B	\N	0	0	0	100	0	f
TWY3-2	TWY3	\N	0	0	0	100	0	f
TWY3-1	TWY3	\N	0	0	0	100	0	f
B-3	B	\N	0	0	0	100	0	f
B-4	B	\N	0	0	0	100	0	f
B-5	B	\N	0	0	0	100	0	f
B-6	B	\N	0	0	0	100	0	f
N7	N7	\N	0	0	0	100	0	f
N8	N8	\N	0	0	0	100	0	f
TWY4-1	TWY4	\N	0	0	0	100	0	f
TWY4-2	TWY4	\N	0	0	0	100	0	f
Main TWY1-4	Main TWY1	\N	0	0	0	100	0	f
Main TWY1-5	Main TWY1	\N	0	0	0	100	0	f
Main TWY1-6	Main TWY1	\N	0	0	0	100	0	f
TWY10	TWY10	\N	0	0	0	100	0	f
TWY18	TWY18	\N	0	0	0	100	0	f
Main TWY1-7	Main TWY1	\N	0	0	0	100	0	f
Main TWY1-8	Main TWY1	\N	0	0	0	100	0	f
TWY-19	TWY-19	\N	0	0	0	100	0	f
TWY-20	TWY-20	\N	0	0	0	100	0	f
TWY-5	TWY-5	\N	0	0	0	100	0	f
TWY-15	TWY-15	\N	0	0	0	100	0	f
TWY-26-1	TWY-26	\N	0	0	0	100	0	f
TWY-26-2	TWY-26	\N	0	0	0	100	0	f
TWY-14	TWY-14	\N	0	0	0	100	0	f
TWY24	TWY24	\N	0	0	0	100	0	f
Main TWY2-1	Main TWY2	\N	0	0	0	100	0	f
TWY33-1	TWY33	\N	0	0	0	100	0	f
D1	D1	\N	0	0	0	100	0	f
D4	D4	\N	0	0	0	100	0	f
D5	D5	\N	0	0	0	100	0	f
D2	D2	\N	0	0	0	100	0	f
TWY34	TWY34	\N	0	0	0	100	0	f
TWY22	TWY22	\N	0	0	0	100	0	f
L1-1	L1	\N	0	0	0	100	0	f
Main TWY2-2	Main TWY2	\N	0	0	0	100	0	f
Diag1	Diag1	\N	0	0	0	100	0	f
L1-3	L1	\N	0	0	0	100	0	f
Main TWY2-3	Main TWY2	\N	0	0	0	100	0	f
D3-1	D31	\N	0	0	0	100	0	f
D3-2	D3	\N	0	0	0	100	0	f
L1-4	L1	\N	0	0	0	100	0	f
L1-5	L1	\N	0	0	0	100	0	f
F1	F1	\N	0	0	0	100	0	f
F2	F2	\N	0	0	0	100	0	f
TWY35	TWY35	\N	0	0	0	100	0	f
TWY27	TWY27	\N	0	0	0	100	0	f
Main TWY2-4	Main TWY2	\N	0	0	0	100	0	f
TWY27-1	TWY27	\N	0	0	0	100	0	f
TWY27-2	TWY27	\N	0	0	0	100	0	f
Main TWY2-5	Main TWY2	\N	0	0	0	100	0	f
Main TWY2-6	Main TWY2	\N	0	0	0	100	0	f
TWY23	TWY23	\N	0	0	0	100	0	f
TWY28	TWY28	\N	0	0	0	100	0	f
L1-6	L1	\N	0	0	0	100	0	f
L1-7	L1	\N	0	0	0	100	0	f
L1-8	L1	\N	0	0	0	100	0	f
L1-9	L1	\N	0	0	0	100	0	f
TWY28-1	TWY28	\N	0	0	0	100	0	f
diag2	diag2	\N	0	0	0	100	0	f
Main TWY2-7	Main TWY2	\N	0	0	0	100	0	f
Main TWY2-8	Main TWY2	\N	0	0	0	100	0	f
Main TWY2-9	Main TWY2	\N	0	0	0	100	0	f
TWY29	TWY29	\N	0	0	0	100	0	f
F3-1	F3	\N	0	0	0	100	0	f
F3-2	F3	\N	0	0	0	100	0	f
TWY30	TWY30	\N	0	0	0	100	0	f
73	73	\N	0	0	0	100	0	f
diag3	diag3	\N	0	0	0	100	0	f
Main TWY2-10	Main TWY2	\N	0	0	0	100	0	f
Main TWY2-11	Main TWY2	\N	0	0	0	100	0	f
TWY31	TWY31	\N	0	0	0	100	0	f
L2-1	L2	\N	0	0	0	100	0	f
81ABC	81ABC	\N	0	0	0	100	0	f
L2-2	L2	\N	0	0	0	100	0	f
Main TWY2-12	Main TWY2	\N	0	0	0	100	0	f
Main TWY2-13	Main TWY2	\N	0	0	0	100	0	f
Main TWY2-14	Main TWY2	\N	0	0	0	100	0	f
obezd	obezd	\N	0	0	0	100	0	f
TWY2-0	TWY2	\N	0	0	0	100	0	f
TWY12-0	TWY12	\N	0	0	0	100	0	f
TWY13-0	TWY13	\N	0	0	0	100	0	f
N6	N6	\N	0	0	0	100	0	f
\.


--
-- TOC entry 3080 (class 0 OID 17036)
-- Dependencies: 227
-- Data for Name: Users; Type: TABLE DATA; Schema: public; Owner: knova
--

COPY public."Users" (user_id, "Name", "Port") FROM stdin;
1	vasya	10001
2	ilya	10002
3	petya	10003
4	epistaphy	10004
5	avdotya	10005
6	klavdia	10006
7	akakiy	10007
8	dobrynya	10008
9	svyatoslav	10009
10	petrovich	10010
\.


--
-- TOC entry 3079 (class 0 OID 17030)
-- Dependencies: 226
-- Data for Name: Weather; Type: TABLE DATA; Schema: public; Owner: knova
--

COPY public."Weather" (id, temp, temp1, temp2, temp3, temp4, prec_type, prec_type1, prec_type2, prec_type3, prec_type4, prec_strength, prec_strength1, prec_strength2, prec_strength3, prec_strength4) FROM stdin;
1	10	9	8	7	7	0	0	0	0	0	0	0	0	0	0
\.


--
-- TOC entry 3097 (class 0 OID 0)
-- Dependencies: 213
-- Name: Car_id_car_seq; Type: SEQUENCE SET; Schema: public; Owner: knova
--

SELECT pg_catalog.setval('public."Car_id_car_seq"', 1, false);


--
-- TOC entry 3098 (class 0 OID 0)
-- Dependencies: 207
-- Name: Car_type_id_type_seq; Type: SEQUENCE SET; Schema: public; Owner: knova
--

SELECT pg_catalog.setval('public."Car_type_id_type_seq"', 1, false);


--
-- TOC entry 3099 (class 0 OID 0)
-- Dependencies: 223
-- Name: Coordinates_id_c_seq; Type: SEQUENCE SET; Schema: public; Owner: knova
--

SELECT pg_catalog.setval('public."Coordinates_id_c_seq"', 1, true);


--
-- TOC entry 3100 (class 0 OID 0)
-- Dependencies: 221
-- Name: Ingeneers_id_ing_seq; Type: SEQUENCE SET; Schema: public; Owner: knova
--

SELECT pg_catalog.setval('public."Ingeneers_id_ing_seq"', 1, false);


--
-- TOC entry 3101 (class 0 OID 0)
-- Dependencies: 204
-- Name: Plane_reserve_id_reserve_seq; Type: SEQUENCE SET; Schema: public; Owner: knova
--

SELECT pg_catalog.setval('public."Plane_reserve_id_reserve_seq"', 1, false);


--
-- TOC entry 3102 (class 0 OID 0)
-- Dependencies: 211
-- Name: Rule_cars_id_rc_seq; Type: SEQUENCE SET; Schema: public; Owner: knova
--

SELECT pg_catalog.setval('public."Rule_cars_id_rc_seq"', 1, false);


--
-- TOC entry 3103 (class 0 OID 0)
-- Dependencies: 209
-- Name: Rules_id_rule_seq; Type: SEQUENCE SET; Schema: public; Owner: knova
--

SELECT pg_catalog.setval('public."Rules_id_rule_seq"', 1, false);


--
-- TOC entry 3104 (class 0 OID 0)
-- Dependencies: 217
-- Name: Task_cars_id_tc_seq; Type: SEQUENCE SET; Schema: public; Owner: knova
--

SELECT pg_catalog.setval('public."Task_cars_id_tc_seq"', 1, false);


--
-- TOC entry 3105 (class 0 OID 0)
-- Dependencies: 215
-- Name: Task_id_task_seq; Type: SEQUENCE SET; Schema: public; Owner: knova
--

SELECT pg_catalog.setval('public."Task_id_task_seq"', 1, false);


--
-- TOC entry 3106 (class 0 OID 0)
-- Dependencies: 219
-- Name: Task_tracks_id_tt_seq; Type: SEQUENCE SET; Schema: public; Owner: knova
--

SELECT pg_catalog.setval('public."Task_tracks_id_tt_seq"', 1, false);


--
-- TOC entry 3107 (class 0 OID 0)
-- Dependencies: 225
-- Name: Weather_id_seq; Type: SEQUENCE SET; Schema: public; Owner: knova
--

SELECT pg_catalog.setval('public."Weather_id_seq"', 1, false);


--
-- TOC entry 2900 (class 2606 OID 16987)
-- Name: Car Car_pk; Type: CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Car"
    ADD CONSTRAINT "Car_pk" PRIMARY KEY (id_car);


--
-- TOC entry 2894 (class 2606 OID 16963)
-- Name: Car_type Car_type_pk; Type: CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Car_type"
    ADD CONSTRAINT "Car_type_pk" PRIMARY KEY (id_type);


--
-- TOC entry 2910 (class 2606 OID 17027)
-- Name: Coordinates Coordinates_pk; Type: CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Coordinates"
    ADD CONSTRAINT "Coordinates_pk" PRIMARY KEY (id_c);


--
-- TOC entry 2908 (class 2606 OID 17019)
-- Name: Ingeneers Ingeneers_pk; Type: CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Ingeneers"
    ADD CONSTRAINT "Ingeneers_pk" PRIMARY KEY (id_ing);


--
-- TOC entry 2886 (class 2606 OID 16931)
-- Name: Line Line_pk; Type: CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Line"
    ADD CONSTRAINT "Line_pk" PRIMARY KEY ("Line_name");


--
-- TOC entry 2892 (class 2606 OID 16952)
-- Name: Plane Plane_pk; Type: CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Plane"
    ADD CONSTRAINT "Plane_pk" PRIMARY KEY (plane_id);


--
-- TOC entry 2890 (class 2606 OID 16944)
-- Name: Plane_reserve Plane_reserve_pk; Type: CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Plane_reserve"
    ADD CONSTRAINT "Plane_reserve_pk" PRIMARY KEY (id_reserve);


--
-- TOC entry 2898 (class 2606 OID 16979)
-- Name: Rule_cars Rule_cars_pk; Type: CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Rule_cars"
    ADD CONSTRAINT "Rule_cars_pk" PRIMARY KEY (id_rc);


--
-- TOC entry 2896 (class 2606 OID 16971)
-- Name: Rules Rules_pk; Type: CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Rules"
    ADD CONSTRAINT "Rules_pk" PRIMARY KEY (id_rule);


--
-- TOC entry 2904 (class 2606 OID 17003)
-- Name: Task_cars Task_cars_pk; Type: CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Task_cars"
    ADD CONSTRAINT "Task_cars_pk" PRIMARY KEY (id_tc);


--
-- TOC entry 2902 (class 2606 OID 16995)
-- Name: Task Task_pk; Type: CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Task"
    ADD CONSTRAINT "Task_pk" PRIMARY KEY (id_task);


--
-- TOC entry 2906 (class 2606 OID 17011)
-- Name: Task_tracks Task_tracks_pk; Type: CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Task_tracks"
    ADD CONSTRAINT "Task_tracks_pk" PRIMARY KEY (id_tt);


--
-- TOC entry 2888 (class 2606 OID 16936)
-- Name: Track Track_pk; Type: CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Track"
    ADD CONSTRAINT "Track_pk" PRIMARY KEY ("Track_name");


--
-- TOC entry 2914 (class 2606 OID 17040)
-- Name: Users Users_pk; Type: CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT "Users_pk" PRIMARY KEY (user_id);


--
-- TOC entry 2912 (class 2606 OID 17035)
-- Name: Weather Weather_pk; Type: CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Weather"
    ADD CONSTRAINT "Weather_pk" PRIMARY KEY (id);


--
-- TOC entry 2921 (class 2606 OID 17071)
-- Name: Car Car_fk0; Type: FK CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Car"
    ADD CONSTRAINT "Car_fk0" FOREIGN KEY ("Driver_id") REFERENCES public."Users"(user_id);


--
-- TOC entry 2922 (class 2606 OID 17076)
-- Name: Car Car_fk1; Type: FK CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Car"
    ADD CONSTRAINT "Car_fk1" FOREIGN KEY ("Car_type") REFERENCES public."Car_type"(id_type);


--
-- TOC entry 2928 (class 2606 OID 17106)
-- Name: Ingeneers Ingeneers_fk0; Type: FK CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Ingeneers"
    ADD CONSTRAINT "Ingeneers_fk0" FOREIGN KEY (user_id) REFERENCES public."Users"(user_id);


--
-- TOC entry 2916 (class 2606 OID 17046)
-- Name: Plane_reserve Plane_reserve_fk0; Type: FK CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Plane_reserve"
    ADD CONSTRAINT "Plane_reserve_fk0" FOREIGN KEY ("Plane") REFERENCES public."Plane"(plane_id);


--
-- TOC entry 2917 (class 2606 OID 17051)
-- Name: Plane_reserve Plane_reserve_fk1; Type: FK CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Plane_reserve"
    ADD CONSTRAINT "Plane_reserve_fk1" FOREIGN KEY ("Line_name") REFERENCES public."Line"("Line_name");


--
-- TOC entry 2918 (class 2606 OID 17056)
-- Name: Plane_reserve Plane_reserve_fk2; Type: FK CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Plane_reserve"
    ADD CONSTRAINT "Plane_reserve_fk2" FOREIGN KEY ("Track_name") REFERENCES public."Track"("Track_name");


--
-- TOC entry 2919 (class 2606 OID 17061)
-- Name: Rule_cars Rule_cars_fk0; Type: FK CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Rule_cars"
    ADD CONSTRAINT "Rule_cars_fk0" FOREIGN KEY ("Car_type") REFERENCES public."Car_type"(id_type);


--
-- TOC entry 2920 (class 2606 OID 17066)
-- Name: Rule_cars Rule_cars_fk1; Type: FK CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Rule_cars"
    ADD CONSTRAINT "Rule_cars_fk1" FOREIGN KEY (id_rule) REFERENCES public."Rules"(id_rule);


--
-- TOC entry 2924 (class 2606 OID 17086)
-- Name: Task_cars Task_cars_fk0; Type: FK CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Task_cars"
    ADD CONSTRAINT "Task_cars_fk0" FOREIGN KEY ("Task") REFERENCES public."Task"(id_task);


--
-- TOC entry 2925 (class 2606 OID 17091)
-- Name: Task_cars Task_cars_fk1; Type: FK CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Task_cars"
    ADD CONSTRAINT "Task_cars_fk1" FOREIGN KEY ("Car") REFERENCES public."Car"(id_car);


--
-- TOC entry 2923 (class 2606 OID 17081)
-- Name: Task Task_fk0; Type: FK CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Task"
    ADD CONSTRAINT "Task_fk0" FOREIGN KEY ("Author") REFERENCES public."Users"(user_id);


--
-- TOC entry 2926 (class 2606 OID 17096)
-- Name: Task_tracks Task_tracks_fk0; Type: FK CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Task_tracks"
    ADD CONSTRAINT "Task_tracks_fk0" FOREIGN KEY ("Track") REFERENCES public."Track"("Track_name");


--
-- TOC entry 2927 (class 2606 OID 17101)
-- Name: Task_tracks Task_tracks_fk1; Type: FK CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Task_tracks"
    ADD CONSTRAINT "Task_tracks_fk1" FOREIGN KEY ("Task") REFERENCES public."Task"(id_task);


--
-- TOC entry 2915 (class 2606 OID 17041)
-- Name: Track Track_fk0; Type: FK CONSTRAINT; Schema: public; Owner: knova
--

ALTER TABLE ONLY public."Track"
    ADD CONSTRAINT "Track_fk0" FOREIGN KEY ("Line") REFERENCES public."Line"("Line_name");


-- Completed on 2021-05-22 14:11:03

--
-- PostgreSQL database dump complete
--


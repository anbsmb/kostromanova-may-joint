## О проекте

<b>Резпозитории</b>
1) <a href="https://gitlab.com/anbsmb/kostromanova-may-frontend">Frontend</a> - angular приложение
2) <a href="https://gitlab.com/anbsmb/kostromanova-may-backend">Backend</a> - node js сервер
3) <a href="https://gitlab.com/anbsmb/kostromanova-may-mobile">Mobile</a> (мобильная версия) - react приложение
4) <a href="https://gitlab.com/anbsmb/kostromanova-may-api">API</a> - сервер для анализа данных
5) <a href="https://gitlab.com/anbsmb/kostromanova-may-middleware">Middleware</a> - сервера dash, управление картами пользователей
6) <a href="https://gitlab.com/anbsmb/kostromanova-may-joint">Основной репозиторий</a> 

## Реализованная функциональность

- Система автоматического выявления
участков, требующих очистки, с
использованием гибких правил
- Открытый интерфейс доступа на основе
Rest API и Swagger
Интеграция с внешними сервисами
(Погода, расписание самолетов)
- Обеспечение необходимого уровня
информационной безопасности за счет
разделения внешних и внутренних
сервисов
- Автоматическое развертывание
обновлений
- Работа каждого пользователя в
изолированной среде (Docker and
kubernetes)
- Визуализация на карте leaflet
- Возможность разбиения взлетной
полосы на отдельные сегменты для
повышения уровня контроля за
выполняемой уборкой
- Контроль за перемещением объектов
в реальном времени
- Рабочее место диспетчера
(табличный и картографический
интерфейсы)
- Рабочее место полевого инженера
(мобильное приложение)
- Рабочее место водителя


## Особенности проекта

- Повышение уровня контроля за состоянием
летного поля (уборка снега и наледи)
- Визуализация текущей загруженности всех
ресурсов, перемещающихся по аэропорту
- Прогнозирование состояния летного поля в
зависимости от прогноза погоды
- Повышение эффективности работы полевого
инженера
- Автоматизация постановки заданий на очистку
летного поля в зависимости от его состояния
 
## Используемые технологии

Server1 (backend): <b>NodeJS (express js) </b>
<br>Server2 (api): <b>Flask</b>
<br>Server3 (middleware): <b>Flask, Dash</b>
<br>CI/CD: <b>GitLab</b>
<br>Web ui: <b>Angular js, Bootstrap 4, MdBootstrap</b>
<br>Mobile ui: <b>React Native </b>
<br>Data base: <b>Postgres 12 </b><br>
<br>Контейниризация: <b>Docker</b>

## Ссылки

<h3><a href="https://app.swaggerhub.com/apis/None-stopCoding/Hackathon/1.0.0#/">Описание API (Swagger)</a> </h3>
<h3><a href="http://134.209.236.228/#/main">Демо версия web сайта</a></h2>
<h3><a href="https://drive.google.com/file/d/19CY8qCaMfIHBvt0oNB4Z5BfxBn_XtY7S/view?usp=sharing">Презентация проекта </a> </h3>
<h3><a href="https://www.figma.com/file/GlCY4l7QwUitLxzrfvnkZY/Untitled?node-id=1%3A990">Макеты веб-сайта</a> </h3>
 
## Среда запуска
Требования:
1. Развертывание сервиса на ubuntu (debian-like)
2. СУБД PostgresSQL 12 версии и выше
3. Установленный web-сервер (apache, nginx) для запуска клиенской части, так же подойдет node http-server 
4. Другие зависимости устанавливаются непосредственно в пункте "Установка"

## Установка

Вся информация об запуске в файле <a href="https://gitlab.com/anbsmb/kostromanova-may-joint/-/blob/master/require.txt">require.txt</a> 

## База данных, выполнение миграции

<a href="https://gitlab.com/anbsmb/kostromanova-may-backend/-/blob/master/backup(23.05.2021).sql">Бэкап базы данных</a> нужно восстановить в вашей базе данных, в ней уже находятся все заполненные тестовые данные для тестирования. Условия восстановления бэкапа: schema 'public', db 'postgres'

## Команда разработчиков 

<b>Жукова Дарья</b> UI/UX https://t.me/@CAPTAINOFFLINE<br>
<b>Жуков Евгений</b> Devops https://t.me/@anybodyseemybeard<br>
<b>Жуков Алексей</b> Backend https://t.me/@leshik_lesha<br>
<b>Смирнов Данил</b> Frontend https://t.me/@whitetech_project<br>
<b>Денисов Артём</b> Analyst/Backend/Architector https://t.me/@rufimych
